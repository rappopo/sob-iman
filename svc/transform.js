// based on: https://github.com/designtesbrot/moleculer-sharp/blob/master/src/service.js

const { _, fs, isSet } = require('rappopo-sob').Helper
const sharp = require('sharp')

module.exports = ({ sobr }) => {
  return {
    actions: {
      mix: {
        params: {
          source: [
            { type: 'string' },
            { type: 'object' }
          ],
          tasks: { type: 'array' }
        },
        handler (ctx) {
          const tasks = ctx.params.tasks
          tasks.push(['toFile', ctx.params.dest])
          return this.execTasks(ctx.params.source, tasks)
        }
      },
      thumbnail (ctx) {
        let { width, height, options } = ctx.params
        if (!(isSet(width) || isSet(height))) width = 100
        const tasks = [
          ['resize', width, height, options],
          ['toFile', ctx.params.dest]
        ]
        return this.execTasks(ctx.params.source, tasks)
      }
    },
    methods: {
      execTasks (source, tasks) {
        console.log(source, tasks)
        return Promise.resolve(source)
          .then(source => {
            const stream = _.isString(source) ? fs.createReadStream(source) : source
            const pipe = sharp()
            let resp
            _.each(tasks, task => {
              if (_.isString(task)) pipe[task].apply(pipe)
              else if (_.isArray(task)) {
                if (task[0] === 'toFile') resp = pipe[task[0]].apply(pipe, _.drop(task, 1))
                else pipe[task[0]].apply(pipe, _.drop(task, 1))
              }
            })
            stream.on('error', err => {
              pipe.destroy(err)
            })
            stream.pipe(pipe)
            return resp || pipe
          })
      }
    }
  }
}
