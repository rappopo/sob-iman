const sharp = require('sharp')

module.exports = ({ sobr }) => {
  return {
    actions: {
      metadata: {
        params: {
          input: [
            { type: 'string' },
            { type: 'object' }
          ]
        },
        handler (ctx) {
          return Promise.resolve(ctx.params.input)
            .then(sharp)
            .then(image => image.metadata())
        }
      },
      stats: {
        params: {
          input: [
            { type: 'string' },
            { type: 'object' }
          ]
        },
        handler (ctx) {
          return Promise.resolve(ctx.params.input)
            .then(sharp)
            .then(image => image.stats())
        }
      }
    }
  }
}
